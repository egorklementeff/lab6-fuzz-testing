import {calculateBonuses} from "./bonus-system.js";
const assert = require("assert");


describe('Bonus-system tests', () => {
    let app;
    console.log("Tests started");


    test('Valid standard program test',(done) => {
        let mult = 0.05;
        let bonus = 1;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 1);
	assert.equal(actual, expected);
        done();
    });

    test('Valid premium program test',(done) => {
        let mult = 0.1;
        let bonus = 1;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Premium", 1);
	assert.equal(actual, expected);
        done();
    });

    test('Valid diamond program test',(done) => {
        let mult = 0.2;
        let bonus = 1;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Diamond", 1);
	assert.equal(actual, expected);
        done();
    });

    test('Valid nonsense program test',(done) => {
        let mult = 0;
        let bonus = 1;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("nonsense", 1);
	assert.equal(actual, expected);
        done();
    });

    test('Valid bonus less than 50000',(done) => {
        let mult = 0.05;
        let bonus = 1.5;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 10001);
	assert.equal(actual, expected);
        done();
    });

    test('Valid bonus less than 100000',(done) => {
        let mult = 0.05;
        let bonus = 2;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 50001);
	assert.equal(actual, expected);
        done();
    });

    test('Valid bonus more than 100000',(done) => {
        let mult = 0.05;
        let bonus = 2.5;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 100001);
	assert.equal(actual, expected);
        done();
    });

    test('Valid bonus equal to 10000',(done) => {
        let mult = 0.05;
        let bonus = 1.5;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 10000);
	assert.equal(actual, expected);
        done();
    });

    test('Valid bonus equal to 50000',(done) => {
        let mult = 0.05;
        let bonus = 2;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 50000);
	assert.equal(actual, expected);
        done();
    });

    test('Valid bonus equal to 100000',(done) => {
        let mult = 0.05;
        let bonus = 2.5;
        
	let expected = mult * bonus;
	let actual = calculateBonuses("Standard", 100000);
	assert.equal(actual, expected);
        done();
    });


    console.log('Tests Finished');

});
